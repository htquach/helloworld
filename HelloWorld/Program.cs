﻿// Copyright (c) 2018 Hong Quach

// This source file is licensed under the "MIT License."  
// Please see the LICENSE in this distribution for license terms.

namespace HelloWorld
{
    using System;

    /// <summary>
    /// A simple Testable C# program
    /// </summary>
    public class Program
    {
        public static string ExpectedOutput => "Hello World!";

        /// <summary>
        /// Program entry point
        /// </summary>
        public static void Main()
        {
            Console.WriteLine(ExpectedOutput);
        }
    }
}
