// Copyright (c) 2018 Hong Quach

// This source file is licensed under the "MIT License."  
// Please see the LICENSE in this distribution for license terms.

namespace HelloWorldTest
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using System.IO;

    /// <summary>
    /// Test the HelloWorld program
    /// </summary>
    [TestClass]
    public class MainTest
    {
        /// <summary>
        /// Test the program's main entry point
        /// </summary>
        [TestMethod]
        public void Main_PrintExpectedOutput_True()
        {
            var savedConsoleOut = Console.Out;
            try
            {
                var consoleOut = new StringWriter();
                Console.SetOut(consoleOut);
                Assert.IsFalse(consoleOut.ToString().Contains(HelloWorld.Program.ExpectedOutput));

                HelloWorld.Program.Main();

                StringAssert.Contains(consoleOut.ToString(), HelloWorld.Program.ExpectedOutput);
            }
            finally
            {
                Console.SetOut(savedConsoleOut);
            }
        }
    }
}
